# How to compile this project?
To compile this project use the command `./gradlew assemble` in the root 
directory and wait for build system to finish. If your system is configured 
properly then after a few moments the compilation will finish and you'll be 
able to find the compiled `.apk` in the `build/` folder.
